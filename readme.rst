buildcounter
------------

buildcounter is a small automatic (and ISC-licensed) tool that helps keeping track of different builds for projects in C/C++. It can be used in makefiles or as part of any project that supports custom post-build steps (like Visual Studio). It works by creating and modifying a header file that contains a define like this:

``#define BUILDNO 17``

buildcounter knows this parameters:

\-s
    Silent mode; this does not output the new contents to stdout
\-\-version, -h or --help
    Print the usage with versioning information and link to this repository
\-\-
    Use this parameter to use one of the other parameter names as the output filename (do you really want that?)

Do **not** change the contents of the output file manually. buildcounter will not overwrite files that do not begin with the expected ``#define BUILDNO <any number>\n`` and any changes after that line will be overwritten.

Using buildcounter
..................

To use **buildcounter** for your project, do this:

1
   In your source folder, call **buildcounter buildno.h** manually. As this creates an internal header file, it should not go into a special include folder.
2
   Create a target called "buildno.h" in your makefile. If you do not have a default target containing only dependencies yet, create one. Make it dependend to all necessary build steps and add the "buildno.h" to the end.

*Example:*

::

    all: withoutcounting buildno.h

    withoutcounting: a.out

    a.out: test.c
        gcc test.c

    buildno.h: test.c
        buildcounter -s buildno.h

3
   In your code, include "buildno.h" and use **BUILDNO**. Build your code normally using the default target (or explicitly call "make all"). If you do not want to increase the counter after a specific build, call "make withoutcounting".

Remarks
.......

Finding the right place to call buildcounter allows some easy mistakes.
It should only be called once for each successful build. That means that you should call it after the real building.
If you forget to make it dependend of any of your source files, buildcounter will be called even if there where no changes at all. If you only add some but not all source files, changes of other files will not change the counter.


License
.......

Copyright (c) 2012, <b.w (a) gmx (.) tm>

Permission to use, copy, modify, and/or distribute this software for any purpose
with or without fee is hereby granted, provided that the above copyright notice
and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS
OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF
THIS SOFTWARE.



