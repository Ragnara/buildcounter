CC=clang 
CCWIN=i486-mingw32-gcc
CFLAGS=-std=c11 -g -Wall -Wextra -Werror -pedantic

all: buildcounter.exe buildcounter buildno.h

buildcounter: buildcounter.c 
	$(CC) $(CFLAGS) -o buildcounter buildcounter.c

buildcounter.exe: buildcounter.c 
	$(CCWIN) $(CFLAGS) -o buildcounter.exe buildcounter.c	

buildcounter.exe.zip: buildcounter.exe
	zip buildcounter.exe.zip buildcounter.exe

manpage: buildcounter.1
	cat buildcounter.1 | gzip >buildcounter.1.gz

buildno.h: buildcounter.c
	@buildcounter -s buildno.h || echo "No buildcounting"

clean:
	-rm buildcounter.exe
	-rm buildcounter
	-rm buildcounter.exe.zip
	-rm buildcounter.1.gz


.PHONY: clean


